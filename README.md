# LeaRnV: Teaching Integrated Systems using RISC-V

Welcome to the official leaRnV repository.
To visualize the page please refer to  https://tima-amfors.gricad-pages.univ-grenoble-alpes.fr/learnv/ 


#Resources

Here is the list of resources you will need to start the course.
All these documents are available in the Download folder, or through links to a drive given below.

- demo_presentation.pdf : PDF file containing the presentation of the demo.

- environment_setup.pdf : PDF file containing a manual to setup the working environment.
						  In particular, you will find in this file how and where toinstall the different archives listed below.

- lab_subject.pdf : PDF file containing all the subjects of the practical labs, as well as all the documentation required to complete these labs.

- lowrisc_project_subject.pdf : PDF file containing a full description of the project.

- lowRisc_project.pdf : PDF file containing the technical manual and documentation associated with the project, necessary to understand the hardware part of it.

- TPSoC_3A_Sys_2020.zip : Archive containing the Virtual Disk.
						  Available to download here : https://drive.google.com/drive/folders/1Fi2GBHrDvF2dkOjpuDXdv6pR3x8JrTVv?usp=sharing

- TPSoC_Resources.tar.xz : Archive containing tools like Rocket-Chip or the RISC-V toolchain.
						Available to download here : https://drive.google.com/drive/folders/1Fi2GBHrDvF2dkOjpuDXdv6pR3x8JrTVv?usp=sharing

- TP_Code_Source.zip : Archive containing the templates of the labs.
					   Available to download here : https://drive.google.com/drive/folders/1Fi2GBHrDvF2dkOjpuDXdv6pR3x8JrTVv?usp=sharing

- lowrisc-chip-DATE2020-Student : Archive containing the project template.
								  Available to download here : https://drive.google.com/drive/folders/1hrl2jdR2Vth-LHSbGfdPNsoaX_dAuQHE?usp=sharing

# License

??
